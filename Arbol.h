#include <iostream>
// Nodo
#include "programa.h"

#ifndef ARBOL_H
#define ARBOL_H

class Arbol{
    private:

    public:
      // Constructor y funciones
      Arbol();
      Nodo *crear_Nodo(int n, Nodo *padre);
      void insertarNodo(Nodo *&raiz, int n, Nodo *padre);
      void mostrar_preOrden(Nodo *raiz);
      void mostrar_inOrden(Nodo *raiz);
      void mostrar_posOrden(Nodo *raiz);
      void buscar_eliminar(Nodo *raiz, int n);
      void eliminarNodo(Nodo *nodo_eliminar);
      Nodo* nodo_mas_izquierdo(Nodo *raiz);
      void reemplazar(Nodo *raiz, Nodo *nodo_cambio);
      void borrar_nodo(Nodo *nodo);
};
#endif
