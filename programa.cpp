#include <iostream>
#include <fstream>
#include "Arbol.h"

using namespace std;

class Grafico {
    private:
        Nodo *arbol = NULL;

    public:
        // Constructor de la Clase Grafico.
        // Creara el archivo .txt de salida, agregando contenido del arbol
        // para generar la imagen (grafica del arbol)
        Grafico(Nodo *raiz) {
	        this->arbol = raiz;
        }

        // Se recorre el arbol de forma preorden, agregando los datos al archivo
        void recorre_Arbol(Nodo *node, ofstream &grafica) {
	        string cadena;
	        if (node != NULL) {
	        if (node->izq != NULL) {
		        grafica<< node->dato << "->" << node->izq->dato << ";" << endl;
	        }

	        else {
		        cadena = to_string(node->dato) + "i";
		        cadena = "\"" + cadena + "\"";
		        grafica << cadena << "[shape=point]" << endl;
		        grafica << node->dato << "->" << cadena << ";" << endl;
	        }

	        cadena = node->dato;

	        if (node->der != NULL) {
		        grafica << node->dato << "->" << node->der->dato << ";" << endl;
	        }

	        else {
		        cadena = to_string(node->dato) + "d";
		        cadena = "\"" + cadena + "\"";
		        grafica << cadena << "[shape=point]" << endl;
		        grafica << node->dato << "->" << cadena << ";" << endl;
	        }

	        // Creación del grafico por izquierda y por derecha
	        recorre_Arbol(node->izq, grafica);
	        recorre_Arbol(node->der, grafica);
	        }

	        return;
        }

        void crea_Grafico() {
	        ofstream grafica;
          // Abre el archivo
	        grafica.open("grafo.txt");
	        grafica << "digraph G {" << endl;
	        grafica << "node [style=filled fillcolor=red];" << endl;
	        recorre_Arbol(this->arbol, grafica);
	        grafica << "}" << endl;
          // Cierra el archivo
	        grafica.close();

	        // Genera el gráfico
	        system("dot -Tpng -ografo.png grafo.txt &");
          // Visualiza el gráfico
	        system("eog grafo.png &");
        }
};

void insertar(Arbol *arbol_nuevo, Nodo *&raiz){
  string numero;

  Nodo *nodo = NULL;

  system("clear");

  cout << "\n Digite un numero: " << endl;
  cin.ignore();
  getline(cin, numero);

  nodo = arbol_nuevo->crear_Nodo(stoi(numero), NULL);

  // primer dato ingresado es la raiz
  if(raiz == NULL){
    raiz = nodo;
  }
  else{
    // no es el primero nodo ingresado
    arbol_nuevo->insertarNodo(raiz, stoi(numero), NULL);
  }
}

void eliminar_dato(Arbol *arbol_nuevo, Nodo *raiz){
  string eliminado;

  system("clear");

  cout << "Ingrese numero a eliminar: " << endl;
  cin.ignore();
  getline(cin, eliminado);

  arbol_nuevo->buscar_eliminar(raiz, stoi(eliminado));
}

void recorrer_arbol(Arbol *arbol_nuevo, Nodo *raiz){
  int orden;

  system("clear");

  do {
      cout << "Formas de recorrer el arbol" << endl;
      cout << " 1. Preorden" << endl;
      cout << " 2. Inorden" << endl;
      cout << " 3. Posorden" << endl;
      cout << " 0. Regresar al menú principal" << endl;

      cout << "Ingrese su elección:" << endl;
      cin >> orden;

      if(raiz == NULL){
        cout << "El arbol no contiene elementos" << endl;
      }

      else{
        if(orden == 1){
          cout << "Arbol en Preorden" << endl;
          arbol_nuevo->mostrar_preOrden(raiz);
          cout << ("\n");
        }

        else if(orden == 2){
          cout << "Arbol en Inorden" << endl;
          arbol_nuevo->mostrar_inOrden(raiz);
          cout << ("\n");
        }
        else if(orden == 3){
          cout << "Arbol en Posorden" << endl;
          arbol_nuevo->mostrar_posOrden(raiz);
          cout << ("\n");
        }
      }

  } while(orden != 0);
}

// Menu de interacción para el programa
void menu(Arbol *arbol_nuevo, Nodo *raiz){
  int op;

  do {
    cout << "\t:::MENU:::" << endl;
    cout << "1. Ingresar número" << endl;
    cout << "2. Eliminar un elemento" << endl;
    cout << "3. modificar elemento" << endl;
    cout << "4. Mostrar contenido arbol" << endl;
    cout << "5. Generar grafo del arbol" << endl;
    cout << "6. Salir" << endl;

    cout << "Ingrese su opcion" << endl;
    cin >> op;

    switch (op) {
        case 1:
            // Crea el nodo y lo agrega al arbol
            insertar(arbol_nuevo, raiz);
            break;

        case 2:
            // eliminación del nodo elegido
            eliminar_dato(arbol_nuevo, raiz);
            break;

        case 3:
            // Modifica un nodo, problemas al momento de ingresar uno nuevo
            eliminar_dato(arbol_nuevo, raiz);
            insertar(arbol_nuevo, raiz);
            break;

        case 4:
            // Formas de recorrer el arbol
            recorrer_arbol(arbol_nuevo, raiz);
            break;

        case 5:
            // Representación gráfica del arbol binario
            Grafico *g = new Grafico(raiz);
            g->crea_Grafico();
            break;
    }

    system("clear");

  } while(op != 6);
}

int main(int argc, char const *argv[]) {
  Arbol *arbol_nuevo = new Arbol();
  Nodo *raiz = NULL;

  menu(arbol_nuevo, raiz);

  return 0;
}
