# Arboles binarios
Se solicita realizar un programa que cree un árbol binario de búsqueda que contenga solo número enteros,
efectuando operaciones básicas en este, como: insertar número, eliminar número buscado, modificar un elemento
(eliminar valor viejo e insertar uno nuevo), mostrar contenido del árbol generado de las formas preorden,
inorden y posorden, y finalmente generar un grafo correspondiente a la estructura creada.

# Como funciona
El programa inicia con un menú general en el cual se presentan 6 situaciones posibles de interacción 
de las cuales 5 están ejecutables:
-Opción 1: Corresponde a la inserción de un nuevo número (dato) en el árbol generado.

-Opción 2: Permite al usuario eliminar un número que contenga el arbol.

-Opción 3: (no funcional del todo): Esta opción teóricamente permite al usuario modificar un número como bien
se describió en la solicitud del ejercicio, sin embargo al ingresar un valor no lo hace de la manera correcta,
ingresando otro valor que no corresponde al ingresado por el ususario. Presentando este fallo se puede calificar
a esta función como no habilitada o no funcional completamente.

-Opción 4: Muestra el contenido del árbol generado con un mini-menú de opciones, en este
se presentan 4 posibilidades: (1)mostrar datos de forma preorden, (2)mostrar datos de forma inorden, 
(3)mostrar datos de forma posorden y finalmente la opción (0)vuelta al menú principal.

-Opción 5: Genera un archivo .txt y a la vez crea una imagen de este, el cual contiene la información asociada
al ingreso de números y la estructura del árbol creado.

-Opción 6: Permite al usuario finalizar el programa.

# Para ejecutar
Se debe descargar la carpeta con los respectivos archivos .cpp y .h, luego abrir la terminal en la carpeta donde
se almacenaron archivos donde se almacenaron. Prontamente se debe ejecutar el comando make en la terminal para 
la respectiva compilación de los archivos y luego ejecutar el comando ./programa para iniciar la aplicación y
la respectiva interacción.

# Construido con 

- sistema operativo (SO): Ubuntu

- lenguaje de programación: C++

- libreria(s): iostream, fstream

- editor de texto: Atom

# Versiones

- Ubuntu 20.04 LTS

- Atom 1.52.0

# Autor

- Martín Muñoz Vera 
