#include <iostream>
#include "Arbol.h"

using namespace std;

Arbol::Arbol(){

}

// crea un nodo
Nodo* Arbol::crear_Nodo(int n, Nodo *padre){
  Nodo *temp = new Nodo();

  // asigna valor al nodo
  temp->dato = n;

  // se generan sub-arboles izquierdo y derecho
  temp->izq = NULL;
  temp->der = NULL;
  temp->nodo_padre = padre;

  return temp;
}

// inserta el nodo donde corresponda, segun valor
void Arbol::insertarNodo(Nodo *&raiz, int n, Nodo *padre){

  if(raiz == NULL){
    Nodo *temp;
    temp = crear_Nodo(n, padre);
    raiz = temp;
    insertarNodo(raiz, n, padre);
  }

  // compara valor de la raiz con el valor del dato ingresado
  else{
    int valor_raiz = raiz->dato;
    if(n < valor_raiz){
      // si es menor a la raiz, se ingresara a la izquierda
      insertarNodo(raiz->izq, n, raiz);
    }
    else if(n > valor_raiz){
      // si es mayor se ingresara a la derecha
      insertarNodo(raiz->der, n, raiz);
    }
  }
}

// se reemplaza nodo a eliminar por el descendiente a este
void Arbol::reemplazar(Nodo *raiz, Nodo *nodo_cambio){
  if(raiz->nodo_padre){
        // cambiar por el izquierdo
        if(raiz->dato == raiz->nodo_padre->izq->dato){
            raiz->nodo_padre->izq = nodo_cambio;
        }

        // cambiar por el derecho
        else if(raiz->dato == raiz->nodo_padre->der->dato){
            raiz->nodo_padre->der = nodo_cambio;
        }
    }

    // asigna padre del nuevo nodo por el que se elimina
    if(nodo_cambio){
        nodo_cambio->nodo_padre = raiz->nodo_padre;
    }
}

// Analizar el nodo a eliminar si es padre u hoja
void Arbol::eliminarNodo(Nodo *nodo_eliminar){

  // Si tiene 2 hijos
  if(nodo_eliminar->izq  && nodo_eliminar->der){
        // Se busca nodo hijo de la derecha y luego mas a la izquierda para reemplazarlo
        Nodo *menor = nodo_mas_izquierdo(nodo_eliminar->der);
        nodo_eliminar->dato = menor->dato;
        eliminarNodo(menor);
    }

    // Si tiene 1 hijo a la izquierda
    else if(nodo_eliminar->izq){
        reemplazar(nodo_eliminar, nodo_eliminar->izq);
        borrar_nodo(nodo_eliminar);
    }

    // Si tiene 1 hijo a la derecha
    else if(nodo_eliminar->der){
        reemplazar(nodo_eliminar, nodo_eliminar->der);
        borrar_nodo(nodo_eliminar);
    }

    // Es hoja
    else{
        reemplazar(nodo_eliminar, NULL);
    }
}

// Elimina un nodo del arbol
void Arbol::buscar_eliminar(Nodo *raiz, int n){
  if(raiz == NULL){
    return;
  }
  // Si es menor el numero
  else if(n < raiz->dato){
    buscar_eliminar(raiz->izq, n);
  }
  // Si es mayor el numero
  else if(n > raiz->dato){
    buscar_eliminar(raiz->der, n);
  }
  else{
    eliminarNodo(raiz);
  }
}

// Nodo no tiene ni izquierdo ni derecho, por tanto se borra
void Arbol::borrar_nodo(Nodo *nodo){
  nodo->izq = NULL;
  nodo->der = NULL;
}

// busqueda para cumplir modo de eliminacion en arboles binarios
Nodo* Arbol::nodo_mas_izquierdo(Nodo *raiz){
  if(raiz == NULL){
    return NULL;
  }
  else if(raiz->izq){
    return nodo_mas_izquierdo(raiz->izq);
  }
  else{
    return raiz;
  }
}

void Arbol::mostrar_preOrden(Nodo *raiz){
  if(raiz == NULL){
    return;
  }
  else{
    cout << raiz->dato << "-";
    mostrar_preOrden(raiz->izq);
    mostrar_preOrden(raiz->der);
  }
}

void Arbol::mostrar_inOrden(Nodo *raiz){
  if(raiz == NULL){
    return;
  }
  else{
    mostrar_inOrden(raiz->izq);
    cout << raiz->dato << "-";
    mostrar_inOrden(raiz->der);
  }
}

void Arbol::mostrar_posOrden(Nodo *raiz){
  if(raiz == NULL){
    return;
  }
  else{
    mostrar_posOrden(raiz->izq);
    mostrar_posOrden(raiz->der);
    cout << raiz->dato << "-";
  }
}
